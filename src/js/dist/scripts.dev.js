"use strict";

// random slider pic
var choosePic = function choosePic() {
  var slides = ['slider-1.png', 'slider-2.png', 'slider-3.png', 'slider-4.png', 'slider-5.png'];
  var randomSlide = slides[Math.floor(Math.random() * slides.length)];
  var sliderImg = document.createElement('img');
  sliderImg.src = "./img/".concat(randomSlide);
  var slider = document.getElementById('slider');
  slider.appendChild(sliderImg);
};

window.onload = choosePic; // Read more/ read less 

document.querySelectorAll('.btn-more').forEach(function (item) {
  item.addEventListener('click', function (event) {
    var parent = item.parentElement;
    var text = parent.childNodes[5];
    var dots = text.childNodes[1];
    var moreText = text.childNodes[2];

    if (moreText.style.display === 'inline') {
      item.innerHTML = 'Read more';
      moreText.style.display = 'none';
      dots.style.display = 'inline';
    } else {
      console.log('not-inline');
      moreText.style.display = 'inline';
      item.innerHTML = 'Read less';
      dots.style.display = 'none';
    }
  });
}); // Pagination

var posts = Array.from(document.querySelectorAll('.post'));
var posts_wrapper = document.getElementById('postsItems');
var btns_wrapper = document.getElementById('pagination');
var current_page = 1;
var posts_per_page = 3;
var pages_count = Math.ceil(posts.length / posts_per_page);
var btns_per_page = 3;

var postsList = function postsList(page) {
  posts_wrapper.innerHTML = '';
  var first_post = (page - 1) * posts_per_page;
  var last_post = first_post + posts_per_page;
  var posts_list = posts.slice(first_post, last_post);
  posts_list.map(function (post) {
    return posts_wrapper.appendChild(post);
  });
};

var pagination = function pagination(page) {
  btns_wrapper.innerHTML = '';

  for (var i = 1; i < pages_count + 1; i++) {
    var btn = PageBtn(i);
    btns_wrapper.appendChild(btn);
  }
};

var PageBtn = function PageBtn(page) {
  var button = document.createElement('button');
  button.classList.add('pagination-element');
  button.innerHTML = page;

  if (page === current_page) {
    button.classList.add('current');
  }

  button.addEventListener('click', function () {
    current_page = page;
    postsList(page);
    document.querySelector('.current').classList.remove('current');
    button.classList.add('current');
    document.getElementById('currentPageNumber').innerHTML = "Page ".concat(page, " from 5");
  });
  document.getElementById('nextBtn').addEventListener('click', function () {
    postsList(pages_count);
    current_page = pages_count;
    document.querySelector('.current').classList.remove('current');
    button.classList.add('current');
    document.getElementById('currentPageNumber').innerHTML = "Page ".concat(page, " from 5");
  });
  return button;
};

postsList(current_page);
pagination(current_page); // search 

var searchBar = document.getElementById('searchBar');
var searchBtn = document.getElementById('searchBtn');
searchBtn.addEventListener('click', function (e) {
  e.preventDefault();
  var text = searchBar.value;

  if (!text) {
    alert('Please enter some text to search!');
    return;
  }

  if (!window.find(text)) {
    alert("The following text was not found: ".concat(text));
  }

  var search = new RegExp("(\\b" + text + "\\b)", "gim");
  var searchArea = document.getElementById('postsItems').innerHTML;
  var searchAreaNew = searchArea.replace(/(<mark>|<\/mark>)/igm, "");
  document.getElementById('postsItems').innerHTML = searchAreaNew;
  var result = searchAreaNew.replace(search, '<mark class="mark">$1</mark>');
  document.getElementById('postsItems').innerHTML = result;
});